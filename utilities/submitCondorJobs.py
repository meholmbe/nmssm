#!/usr/bin/env python3
import os
import argparse
import datetime
import subprocess

# Script to submit NMSSM PostProcessor jobs to HTCondor
# TO DO: make it split into several smaller jobs like in Sam's original script.

# To be run from within the NMSSM/ folder:
#    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -i <INPUT> -O <outputDir> -n <maxEntries>


# Argument Parser
parser = argparse.ArgumentParser(description='Submits NMSSM PostProcessor jobs to HTCondor.')
# General flags
parser.add_argument("--runPostProc", action="store_true", help="Submit a post processing job.")
parser.add_argument("--runAnalysis", action="store_true", help="Submit an analysis job.")
parser.add_argument("-i", "--input", default=None, nargs="+", help="Specifies the input file(s). Separate multiple root file inputs with a space, or use a text file with the data file names (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default=".", help="Name of output directory. (default = %(default)s)")
parser.add_argument("-n", "--maxEntries", default=None, type=int, help="Maximum number of events to process, per input file... (default = %(default)s)")
parser.add_argument("--isData", action="store_true", help="If input is data. If not specified, then MC events are assumed.")
# Postproc flags
parser.add_argument("-y", "--year", default=None, type=str, help="The year of the input files. Only for postprocessing!")
# Analysis flags
parser.add_argument("-I", "--inputDir", default="", help="Directory to the input files. Will take all matching files in the specified folder as input. Cannot be used at the same time as --input. Only for analysis! (default = %(default)s)")
parser.add_argument("--massSelectROC", action="store_true", help="If we want to run a seperate analysis that saves values for ROC curves after mass event selection. Only for analysis!")
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, btagHbb and/or particleNet, or all for all implemented b-taggers. (default = %(default)s)")

args = parser.parse_args()


# Returns a dictionary with all the configuration settings
def set_configs(args):

    # Check that input arguments are valid
    if args.runPostProc and args.runAnalysis:
        raise ValueError("Can not run post processing and analysis script at the same time.")
    elif not args.runPostProc and not args.runAnalysis:
        raise ValueError("Please specify one job type to run (--runPostProc or --runAnalysis).")
    elif args.runPostProc and args.year is None:
        raise ValueError("A year must be specified for post processing (--year).")
    elif args.runPostProc and args.input is None:
        raise ValueError("Input must be specified for post processing (--input).")

    # Check that the taggers exist
    btagger_list = ["particleNet", "btagHbb"]
    if "all" in args.taggers:
        args.taggers = btagger_list
    else:
        for t in args.taggers:
            if t not in btagger_list:
                raise Exception("The b-tagger(s) are not defined: %s" % t)

    cfg = {}
    # Job directories and file names
    cfg["working_area"] = os.getcwd()
    cfg["nmssm_area"] = "python/postprocessing/NMSSM/"
    cfg["nmssm_job"] = "postProcNMSSM.py" if args.runPostProc else "analysisNMSSM.py"
    cfg["log_name"] = "job$(Process)"
    cfg["condor_subfile"] = "condor_submit_file"
    cfg["job_script"] = "condor_job_script.sh"
    # General arguments
    cfg["runPostProc"] = args.runPostProc
    cfg["runAnalysis"] = args.runAnalysis
    if args.input is None:
        cfg["input"] = args.input
    else:
        cfg["input"] = " ".join(args.input)
    if args.outputDir[0] == "/": # An absolute path
        cfg["outputDir"] = args.outputDir + "_" + datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    else: # Relative path
        cfg["outputDir"] = cfg["working_area"] + "/" + args.outputDir + "_" + datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    cfg["maxEntries"] = args.maxEntries
    cfg["year"] = args.year
    # The PostProc arguments
    cfg["isData"] = args.isData
    # The Analysis arguments
    cfg["inputDir"] = args.inputDir
    cfg["massSelectROC"] = args.massSelectROC
    cfg["taggers"] = args.taggers
    # Check this is run from CMSSW_VERSION/src/PhysicsTools/NanoAODTools
    if cfg["working_area"].split("/")[-1] != "NanoAODTools":
        raise Exception("Script not run from CMSSW_VERSION/src/PhysicsTools/NanoAODTools")

    return cfg

# Make the bash script that sets up the environment and runs the jobs
def make_job_script(cfg):
    # Make the main python command string
    python_cmd = "python3 {nmssm_area}/{nmssm_job} -O {outputDir}  -y {year}".format(**cfg)
    # Add general arguments
    if cfg["maxEntries"]:
        python_cmd += " -n {maxEntries}".format(**cfg)
    if cfg["isData"]:
        python_cmd += " --isData"
    # Add PostProc arguments
    if cfg["runPostProc"]:
        python_cmd += " -i {input}".format(**cfg)
    # Add Analysis arguments
    elif cfg["runAnalysis"]:
        if cfg["input"] is not None:
            python_cmd += " -i {input}".format(**cfg)
        else:
            python_cmd += " --inputDir {inputDir}".format(**cfg)
        # Add which taggers to run over
        python_cmd += " -t " + " ".join(args.taggers)
        if cfg["massSelectROC"]:
            python_cmd += " --massSelectROC"

    # The script body
    text = """
#!/usr/bin/env bash

# Job script for submitting to condor
# Used by condor_submit_file.sh

# Set environment
source ~/.bash_profile
echo $TMPDIR

# Ensure CMSSW environment is set up.
cd {working_area}/../..
eval `scramv1 runtime -sh`
echo $CMSSW_RELEASE_BASE $CMSSW_BASE
cd {working_area}

# Check user proxy
echo $X509_USER_PROXY
voms-proxy-info -all

# Run job
echo Running job...
echo {python_cmd}
{python_cmd}
echo Finished job!
""".format(**cfg, python_cmd=python_cmd)
    return text

# Make the Condor submission file
def make_submit_file(cfg):
    text = """
# Condor submit file
Universe               = vanilla
Executable             = {outputDir}/condor_job_script.sh

# Log files
Log                    = {outputDir}/{log_name}.log
Output                 = {outputDir}/{log_name}_out.log
Error                  = {outputDir}/{log_name}_err.log

# Some OS specifications
Request_memory         = 2.5 GB
requirements           = (OpSysAndVer =?= "CentOS7")
request_cpus           = 1
Getenv                 = False

# User proxy
use_x509userproxy = True

# Queueueueu
queue
""".format(**cfg)
    return text


# Set configurations
cfg = set_configs(args)

# Make output directory
os.makedirs(cfg['outputDir'])

# Make Condor files
condor_subfile = "{outputDir}/{condor_subfile}".format(**cfg)
with open(condor_subfile, "w", encoding="utf-8") as f:
    f.write(make_submit_file(cfg))

job_script = "{outputDir}/{job_script}".format(**cfg)
with open(job_script, "w", encoding="utf-8") as f:
    f.write(make_job_script(cfg))

# Submit to Condor
subprocess.Popen(['condor_submit',condor_subfile]).communicate()
