#!/usr/bin/env python3
import ROOT

from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetHelperRun2 import createJMECorrector
# from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jecUncertainties import *
from PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer import puWeight_UL2017
from PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer import btagSF2017
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.crossSectionScales import CrossSectionScales

# this takes care of converting the input files from CRAB
from PhysicsTools.NanoAODTools.postprocessing.framework.crabhelper import inputFiles, runsAndLumis

ROOT.PyConfig.IgnoreCommandLineOptions = True

# Create a chain of modules to run for the analysis
postproc_chain = []

# Add cross-section scaling
xsec_scale = CrossSectionScales()
postproc_chain.append(xsec_scale)

# Add jet uncertainties modules. Adds JES related branches to the output root file.
# Can't do "UL", missing files and constants used by the module?
jme_corrections_ak4 = createJMECorrector(True, "2017", "B", "Total", "AK4PFPuppi", False) # Can only do one run period at a time. Are the tags for the dicts in jetmetHelperRun2 correct?!
postproc_chain.append(jme_corrections_ak4())

jme_corrections_ak8 = createJMECorrector(True, "2017", "B", "Total", "AK8PFPuppi", False) # Can only do one run period at a time. Are the tags for the dicts in jetmetHelperRun2 correct?!
postproc_chain.append(jme_corrections_ak8())

# Add pileup weight module. What does this one do?
pu_weights = puWeight_UL2017
postproc_chain.append(pu_weights())

# Add b-tag scale factor module. Not all years and b-tag discriminators supported.
btag_sf = btagSF2017
postproc_chain.append(btag_sf())

# Add JEC uncertainties module. Do we need this one? It's crashing
# jec_uncert = jecUncertAll_cppOut
# postproc_chain.append(jec_uncert())

# Something for the PDF? Need to write that oneself...

# Preselection cuts
# PRESELECTION = "FatJet_pt[0] > 170 && FatJet_pt[1] > 170 && abs(FatJet_eta[0]) < 2.4 && abs(FatJet_eta[1]) < 2.4" # Jets seem to be ordered from high to low pt

# Create post processor and run it
p = PostProcessor(".",
                  inputFiles(),
                  # cut=PRESELECTION,
                  modules=postproc_chain,
                  provenance=True,
                  fwkJobReport=True,
                  jsonInput=runsAndLumis())
p.run()
