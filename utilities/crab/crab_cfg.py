#!/usr/bin/env python3
from WMCore.Configuration import Configuration
from CRABClient.UserUtilities import config, getUsername

# Crab Configuration File
# Documentation: https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile

config = Configuration()

config.section_("General")
config.General.requestName = 'NMSSM_postproc'
config.General.transferLogs = True

config.section_("JobType")
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'python/postprocessing/NMSSM/utilities/crab/PSet.py'
config.JobType.scriptExe = 'python/postprocessing/NMSSM/utilities/crab/crab_script.sh'
config.JobType.inputFiles = ['python/postprocessing/NMSSM/utilities/crab/crabPostProc.py', 'scripts/haddnano.py'] # scripts/haddnano.py merges the output trees?
config.JobType.sendPythonFolder = True

config.section_("Data")
config.Data.inputDataset = '/QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL16NanoAODv9-106X_mcRun2_asymptotic_v17-v1/NANOAODSIM'
config.Data.inputDBS = 'global'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 2
config.Data.totalUnits = 2
config.Data.outLFNDirBase = '/store/user/%s/NanoTestPost' % (getUsername())
config.Data.publication = False
config.Data.outputDatasetTag = 'NanoTestPost'

config.section_("Site")
config.Site.storageSite = "T2_UK_SGrid_RALPP"
