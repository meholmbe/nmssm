#!/usr/bin/env python3
import os
import argparse
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors
import mplhep as hep
import hist
from hist import Hist
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# Import variables from plotUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *

# Argument parser
parser = argparse.ArgumentParser(description="Process root file(s) and plot Search Region Bin histograms.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-S", "--signalDirs", default=None, nargs="+", help="Specifies the directories with signal files.")
parser.add_argument("-s", "--signalNames", default=None, nargs="+", help="Specifies the names of the signal files for plot legends.")
parser.add_argument("-B", "--backgroundDirs", default=None, nargs="+", help="Specifies the directories with background files.")
parser.add_argument("-b", "--backgroundNames", default=None, nargs="+", help="Specifies the names of the background files for plot legends")
parser.add_argument("-O", "--outputDir", default="./", help="The output directory, has to already exist. (default = %(default)s)")
parser.add_argument("-o", "--output", default="stacked_histogram.png", help="Name of output file. (default = %(default)s)")
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, either btagHbb or particleNet, or all for both. (default = %(default)s)")

args = parser.parse_args()

# Set more colours
NUM_COLORS = 8
cm = plt.get_cmap("jet")
cNorm  = colors.Normalize(vmin=0, vmax=NUM_COLORS-1)
scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)

HIST_LINEWIDTH = 4
OPACITY = 0.5

matplotlib.rcParams['xtick.major.size'] = 10
matplotlib.rcParams['xtick.minor.size'] = 0
matplotlib.rcParams['xtick.minor.width'] = 0


# Reads in ROOT files in the specified directory, and returns a histogram of the Search Region Bins
def createHistogram(directory, tagger="particleNet"):

    # Get all root files in directory
    files = [directory+"/"+f for f in os.listdir(directory) if (os.path.isfile(directory+"/"+f) and "_Friend_%s.root"%tagger in f)]

    # Open input files
    rdf = ROOT.RDataFrame("Friends", files)
    # rdf.Display("FatJets_signalRegion_%s" % (btag)).Print()

    # Create histogram
    histogram = Hist(hist.axis.Regular(bins=10, start=0.5, stop=10.5, name="bin"))

    # Fill histogram
    branch = "FatJets_signalRegion_%s" % (tagger)
    sr_bins = rdf.AsNumpy([branch])[branch]-0.5 # Get column and turn it into a numpy array (AsNumpy returns a dict). Shift with -0.5 to centre the bins w.r.t. the bin number.
    histogram.fill(sr_bins)

    return histogram

# Creates a stacked histogram of signal and background search regions
def plotSearchRegionBins(signal_dirs, signal_name, background_dirs, background_name, output_dir, output, tagger):

    # Create dictionary of histograms
    signal_dict = {}
    background_dict = {}

    # Create figure
    fig, ax = plt.subplots()

    # Create list of colour indices
    colour_ids = []

    # Process backgrounds
    if background_dirs is not None:

        colour_id = 0 # Keeps track of colours for plotting

        # Read all signal files
        for bkg_dir, bkg_name in zip(background_dirs, background_name):
    
            # Create and add histogram to dict
            background_dict[bkg_name] = createHistogram(bkg_dir, tagger)

            # Add colour index
            colour_ids.append(colour_id)
            colour_id += 1

        # Stack the histograms
        background_stack = hist.Stack.from_dict(background_dict)

    # Process signals
    if signal_dirs is not None:

        colour_id = NUM_COLORS-1 # Keeps track of colours for plotting

        # Read all signal files
        for sig_dir, sig_name in zip(signal_dirs, signal_name):
            
            # Create and add histogram to dict
            signal_dict[sig_name] = createHistogram(sig_dir, tagger)

            # Add colour index
            colour_ids.append(colour_id)
            colour_id -= 1

        # Stack the histograms
        signal_stack = hist.Stack.from_dict(signal_dict)

    # Set colours
    ax.set_prop_cycle(color=[scalarMap.to_rgba(i) for i in colour_ids])

    # Plot figure
    if background_dirs is not None and signal_dirs is not None:
        background_stack[::-1].plot(stack=True, histtype="fill", alpha=OPACITY)
        signal_stack.plot(linewidth=HIST_LINEWIDTH)
    elif background_dirs is not None:
        background_stack[::-1].plot(stack=True, histtype="fill", alpha=OPACITY)
    elif signal_dirs is not None:
        signal_stack.plot(linewidth=HIST_LINEWIDTH)

    # Set axis limit
    plt.xlim(xmin=0.5, xmax=10.5)
    plt.ylim(ymin=0, ymax=None)
    ax.set_xticks(range(1,11,1)) # Set tick labels

    # Set labels
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    ax.set_xlabel("Search Region Bin Number")
    ax.set_ylabel("Events")

    # Set legend
    handles, labels = ax.get_legend_handles_labels() # Hack to change the signal legend linewidth
    for i, handle in enumerate(handles):
        if i >= len(background_dict):
            handles[i] = matplotlib.lines.Line2D([], [], color=scalarMap.to_rgba(colour_ids[i]), linewidth=HIST_LINEWIDTH)
    plt.legend(handles=handles, labels=labels, loc='upper left')

    # Save figure
    plt.savefig(output_dir+"/"+tagger+"_"+output)
    plt.close()

# Check that output directory exists
if not os.path.exists(args.outputDir):
    raise NotADirectoryError("Output directory %s is not a directory." % (args.outputDir))

# Check that the numbers of signa/background names and directories are the same
if len(args.signalDirs) != len(args.signalNames) or len(args.backgroundDirs) != len(args.backgroundNames):
    raise Exception("The number of signal/background names and directories must match.")

# Check that the taggers exist
btagger_list = ["particleNet", "btagHbb"]
if "all" in args.taggers:
    args.taggers = btagger_list
else:
    for t in args.taggers:
        if t not in btagger_list:
            raise Exception("The b-tagger(s) are not defined: %s" % t)
        else: # Run stuff
            plotSearchRegionBins(args.signalDirs, args.signalNames, args.backgroundDirs, args.backgroundNames, args.outputDir, args.output, t)
