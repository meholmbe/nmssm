#!/usr/bin/env python3
import warnings
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import mplhep as hep
# from mpl_toolkits.axes_grid1 import make_axes_locatable

# Error handling
warnings.simplefilter('error', UserWarning)

# Utility functions for plotting

# Set labels. TODO: change this automatically (using a plotting class?)
LLABEL = "Work in Progress" # "Simulation Preliminary"
RLABEL = "13 TeV"

# Set plot style. Thank you Chris.
plt.style.use(hep.style.CMS)

COLORMAP = "jet"

SMALL_SIZE = 20
MEDIUM_SIZE = 25
BIGGER_SIZE = 30

LEGEND_WIDTH = 31
LINEWIDTH = 2

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('axes', linewidth=LINEWIDTH)      # thickness of axes
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['xtick.major.size'] = 20
matplotlib.rcParams['xtick.major.width'] = LINEWIDTH
matplotlib.rcParams['xtick.minor.size'] = 10
matplotlib.rcParams['xtick.minor.width'] = LINEWIDTH - 1

matplotlib.rcParams['ytick.major.size'] = 20
matplotlib.rcParams['ytick.major.width'] = LINEWIDTH
matplotlib.rcParams['ytick.minor.size'] = 10
matplotlib.rcParams['ytick.minor.width'] = LINEWIDTH - 1

############################################
# Plotting functions using Scikit-HEP Hist #
############################################

# Plots one or multiple one-dimensional histograms in the same plot
# NOTE: plot2DHist does something weird with the axes after plotting, so plot any 1D projections before calling plot2DHist
def plot1DHist(hists, scale=None, ylog=False, ylabel=None, legend_labels=None, output_file="hist1d.png"):

    # Check if single or multiple histograms
    if not isinstance(hists, list):
        hists = [hists]

    # Set style
    fig, ax = plt.subplots()
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    plt.gca().set_aspect('auto', adjustable='box')

    # Plot all histograms
    for i, h in enumerate(hists):

        # # Scale histogram
        # if scale:
        #     h = h*scale

        # Set legend labels
        legend_label = legend_labels[i] if legend_labels else None
        # Plot histogram, use different markers?
        h.plot(label=legend_label)

    # Plot legend
    if len(hists) > 1:
        plt.legend()

    # Set axes
    if ylog:
        try:
            plt.yscale('log')
        except UserWarning: # If data has no positive values
            plt.close()
            return False
    else:
        plt.ylim(ymin=0)

    # Set y-label
    ax.set_ylabel(ylabel)

    # Save figure
    plt.savefig(output_file)
    plt.close()
    return True

# Plots a single two-dimensional histogram
# NOTE: plot2DHist does something weird with the axes after plotting, so plot any 1D projections before calling plot2DHist
def plot2DHist(hist, figure_name="", scale=None, cbar_log=False, cbar_label="", output_file="hist2d.png"):

    # Activate existing figure or create new
    if figure_name:
        plt.figure(figure_name)
    else:
        fig, ax = plt.subplots()

    # Check if histogram is empty
    if not hist.sum().value:
        print("Warning: histogram is empty. Will not plot %s" % output_file)
        plt.close()
        return False

    # # Scale histogram
    # if scale:
    #     hist = hist*scale

    # Set style
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    plt.gca().set_aspect('equal', adjustable='box') # Equal scale on x/y-axis. Set as an option?

    # Plot histogram, zero/empty bins becomes white
    bin_min = np.nanmin(hist.values()[np.nonzero(hist.values())]) # Minimum non-zero bin value
    bin_min = min(0.00001, bin_min)
    if cbar_log:
        # Logarithmic colour bar scale
        h = hist.plot(cmin=bin_min, norm=matplotlib.colors.LogNorm())
    else:
        h = hist.plot(cmin=bin_min, vmin=0, vmax=None)

    # Plot colour bar label
    if cbar_label:
        # Ugly hack as I can't seem to access the hist colourbar
        ax.text(1.23, 0.5, cbar_label, size=MEDIUM_SIZE, color='black', rotation='vertical', horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)

    # Save figure
    plt.savefig(output_file)
    plt.close()
    return True

# Plots two histograms in the same plot, and the ratio between them underneath
# Can be extended to plot efficiency as the second plot: https://hist.readthedocs.io/en/latest/user-guide/notebooks/Plots.html
# Uses: https://github.com/scikit-hep/hist/blob/b3210e3feae0b5097f292990a501462a7d7b944c/src/hist/plot.py#L530
def plotRatio(hist_num, hist_denom, ylog=False, ylabel="Counts", num_label=None, denom_label=None, output_file="hist_ratio.png"):

    fig, ax = plt.subplots()
    grid = fig.add_gridspec(2, 1, hspace=0, height_ratios=[3, 1])

    # Set style
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    tmp_ax = plt.gca()
    tmp_ax.set_xticklabels([]) # Remove tick labels
    tmp_ax.set_yticklabels([])

    # Set axes
    main_ax = fig.add_subplot(grid[0])
    subplot_ax = fig.add_subplot(grid[1], sharex=main_ax)
    plt.setp(main_ax.get_xticklabels(), visible=False)

    if ylog:
        main_ax.set_yscale('log')
    else:
        main_ax.set_ylim(ymin=0)

    ax_dict = {"main_ax": main_ax, "ratio_ax": subplot_ax}

    # Plot all histograms
    try:
        main_ax, subplot_ax = hist_num.plot_ratio(hist_denom, ax_dict=ax_dict, fp_label=ylabel, rp_ylabel="Ratio", rp_num_label=num_label, rp_denom_label=denom_label)
    except UserWarning: # Ratio plot empty?
        print("Warning: ratio plot is empty. Will not plot %s" % output_file)
        plt.close()
        return False
    except ValueError: # Bins in the denominator are 0
        print("Warning: some bins in the denominator histogram are 0. Will not plot %s" % output_file)
        plt.close()
        return False

    # Save figure
    plt.savefig(output_file)
    plt.close()
    return True
