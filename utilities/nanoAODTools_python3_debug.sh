#!/bin/bash

# Script that fixes some bugs in NanoAODTools when using python3.
# To be run within the NanoAODTools/ folder
# https://github.com/cms-nanoAOD/nanoAOD-tools/issues/295.

# puWeightProducer.py
filename_pu="python/postprocessing/modules/common/puWeightProducer.py"
search_pu="hist.SetDirectory(None)"
replace_pu="hist.SetDirectory(0)"
sed -i "s/$search_pu/$replace_pu/" $filename_pu

# treeReaderArrayTools.py
filename_tree="python/postprocessing/framework/treeReaderArrayTools.py"
search_tree1="tree._ttreereader = ROOT.TTreeReader(tree, tree._entrylist)"
replace_tree1="tree._ttreereader = ROOT.TTreeReader(tree)"
sed -i "s/$search_tree1/$replace_tree1/" $filename_tree
search_tree2="_ttreereader = ROOT.TTreeReader(tree, getattr(tree, '_entrylist', None))"
replace_tree2="_ttreereader = ROOT.TTreeReader(tree)"
sed -i "s/$search_tree2/$replace_tree2/" $filename_tree

# btagSFProducer.py
filename_btag="python/postprocessing/modules/btv/btagSFProducer.py"
search_btag="self.algo, os.path.join(self.inputFilePath, self.inputFileName))"
replace_btag="self.algo, os.path.join(self.inputFilePath, self.inputFileName), True)"
sed -i "s/$search_btag/$replace_btag/" $filename_btag


# General debugs...

# No UL for fatjets - https://github.com/cms-nanoAOD/nanoAOD-tools/issues/285
filename_fatjet="python/postprocessing/modules/jme/fatJetUncertainties.py"
search_fatjet="self.era = era"
replace_fatjet="self.era = era.replace(\"UL\", \"\")"
sed -i "s/$search_fatjet/$replace_fatjet/" $filename_fatjet
