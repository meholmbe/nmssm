#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import hist
from hist import Hist

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import plot2DHist


# Using the two fatjets chosen to correspond to the b-jets, count the number of events in e.g. signal and control regions.
class MassEventSelection(Module):

    def __init__(self, isData=False, ht_bin_edges=[1500, 2500, 3500], btag="particleNet", btag_sum_cut=1.3, btag_anti_cut=0.3, btag_ctrl_cut=0.8, output_dir="./"):

        self.isData = isData
        self.ht_bin_edges = ht_bin_edges # The left edge of each HT bin. 1500-2500, 2500-3500, 3500+ GeV
        self.btag = btag
        self.btag_sum_cut = btag_sum_cut # Cut for btag region, the sum of the two fatjets b-tag value
        self.btag_anti_cut = btag_anti_cut # Cut for anti-btag region
        self.btag_ctrl_cut = btag_ctrl_cut # Upper cut for control region (lower cut is the anti cut)

        self.output_dir = output_dir + "/" # Output directory for plots

    def beginJob(self):

        # Count number of events passed
        self.nevents_passed_ht = {bin: 0 for bin in self.ht_bin_edges}

        # Set some variable depending on which b-tagger we're using
        if self.btag == "particleNet":
            self.mass = "particleNet_mass"
            self.mass_text = "ParticleNet"
            self.btag_range = [0, 1]
        elif self.btag == "btagHbb":
            self.mass = "msoftdrop"
            self.mass_text = "Softdrop"
            self.btag_range = [-1, 1]
        else:
            raise ValueError("Invalid b-tagger.")

        # Keep track of number of events in signal/sideband regions

        self.df_dict = {} # Dictionary that will contain one event yield dataframe per HT bin
        self.num_signal_regions = 10

        columns=["S", "U", "D", "S_anti", "U_anti", "D_anti", "S_ctrl", "U_ctrl", "D_ctrl", "S_pred", "S_ctrl_pred", "F"]

        for bin in self.ht_bin_edges:
            self.df_dict[bin] = pd.DataFrame(np.zeros((self.num_signal_regions, len(columns))), columns=columns, index=range(1,self.num_signal_regions+1))

        # Define constants and lines for mass event selection

        self.s1_lower_point_x = 40 # x-coordinate for the lowest signal region point (S1)
        self.s1_lower_point_y = 17.6 # y-coordinate

        self.s1_upper_point_x = 51.9
        self.s1_upper_point_y = 26.349732

        self.signal_h = 22.905585 # the vertical/horisontal distance between signal regions, except for S1

        self.signal_angle = 0.1513 # the angle from pi/4 (x=y) of the lines that correspond to the signal region
        self.sideband_angle = self.signal_angle + 0.145 # the lines that correspond to the up/down background regions

        self.sideband_lower_point_x = 64.6751 # x-coordinate for one point of the lower D line
        self.sideband_lower_point_y = 13.574602

        # The y(0) values for the negative lines of the mass regions
        self.mass_regions = [self.s1_lower_point_x + self.s1_lower_point_y] + [(self.s1_upper_point_x + self.s1_upper_point_y) + self.signal_h * n for n in range(self.num_signal_regions)]

        # Equations for the lines that make up the signal and sideband regions
        self.down_line = lambda x: np.tan(np.pi/4 - self.sideband_angle) * (x - self.sideband_lower_point_x) + self.sideband_lower_point_y
        self.signal_down_line = lambda x: np.tan(np.pi/4 - self.signal_angle) * (x - self.s1_lower_point_x) + self.s1_lower_point_y
        self.signal_up_line = lambda x: np.tan(np.pi/4 + self.signal_angle) * (x - self.s1_lower_point_y) + self.s1_lower_point_x
        self.up_line = lambda x: np.tan(np.pi/4 + self.sideband_angle) * (x - self.sideband_lower_point_y) + self.sideband_lower_point_x
        # Lines that make up the n negative mass region line
        self.mass_line = lambda x, n: - x + self.mass_regions[n]
        # Lines for the first sideband region
        self.d1_line = lambda x: (self.sideband_lower_point_y - self.s1_lower_point_y) / (self.sideband_lower_point_x - self.s1_lower_point_x) * (x - self.s1_lower_point_x) + self.s1_lower_point_y
        self.u1_line = lambda x: (self.sideband_lower_point_x - self.s1_lower_point_x) / (self.sideband_lower_point_y - self.s1_lower_point_y) * (x - self.s1_lower_point_y) + self.s1_lower_point_x


        # Plots and histograms
        self.mass_hist_dict = {}
        self.mass_hist_dict_tr = {} # For events that are in the tag region (TR)
        self.mass_hist_dict_anti = {} # For events that are in the anti-tag region
        for bin in self.ht_bin_edges:
            # 2D mass histogram
            self.mass_hist_dict[bin] = Hist(hist.axis.Regular(bins=100, start=0, stop=200, name="x", label="FatJetA %s mass [GeV]" % self.mass_text),
                                                 hist.axis.Regular(bins=100, start=0, stop=200, name="y", label="FatJetB %s mass [GeV]" % self.mass_text),
                                                 storage=hist.storage.Weight()
                                                )
            self.mass_hist_dict_tr[bin] = Hist(hist.axis.Regular(bins=100, start=0, stop=200, name="x", label="FatJetA %s mass [GeV]" % self.mass_text),
                                                 hist.axis.Regular(bins=100, start=0, stop=200, name="y", label="FatJetB %s mass [GeV]" % self.mass_text),
                                                 storage=hist.storage.Weight()
                                                )
            self.mass_hist_dict_anti[bin] = Hist(hist.axis.Regular(bins=100, start=0, stop=200, name="x", label="FatJetA %s mass [GeV]" % self.mass_text),
                                                 hist.axis.Regular(bins=100, start=0, stop=200, name="y", label="FatJetB %s mass [GeV]" % self.mass_text),
                                                 storage=hist.storage.Weight()
                                                )
            # For debug, create scatter plot
            fig, ax = plt.subplots(num="debug" + self.btag + str(bin))


    # Run once at the end
    def endJob(self):

        print("\nTotal number of events that passed the HT bin Mass Event Selction (%s): %i" % (self.btag, sum(self.nevents_passed_ht.values())))

        # Make the plots and compute the predicted number of events in signal bins
        for bin in self.ht_bin_edges:

            # String for specifying the HT bin in the plot title
            lower_edge = bin
            upper_edge_index = list(self.ht_bin_edges).index(bin)
            upper_edge = ("-" + str(self.ht_bin_edges[upper_edge_index+1])) if (upper_edge_index < len(self.ht_bin_edges)-1) else "+"

            # Report on number of events passed
            print("    - HT bin %s%s GeV: %i" % (lower_edge, upper_edge, self.nevents_passed_ht[bin]))

            # Compute predicted number of events in signal region
            for i in range(1, self.num_signal_regions+1):
                try:
                    F = self.df_dict[bin].at[i, "S_anti"] / (self.df_dict[bin].at[i, "U_anti"] + self.df_dict[bin].at[i, "D_anti"])
                except ZeroDivisionError:
                    F = 0

                U = self.df_dict[bin].at[i, "U"]
                D = self.df_dict[bin].at[i, "D"]
                U_ctrl = self.df_dict[bin].at[i, "U_ctrl"]
                D_ctrl = self.df_dict[bin].at[i, "D_ctrl"]

                self.df_dict[bin].at[i, "F"] = F
                self.df_dict[bin].at[i, "S_pred"] = F * (U + D)
                self.df_dict[bin].at[i, "S_ctrl_pred"] = F * (U_ctrl + D_ctrl)

            # For Debug
            pd.set_option('display.max_columns', None)
            pd.set_option('display.expand_frame_repr', False)
            print("\n\nNumber of events in signal and sideband regions, %s, HT bin %s%s GeV:\n" % (self.btag, lower_edge, upper_edge))
            print(self.df_dict[bin], "\n")
            print("Sum of all bins:")
            print(self.df_dict[bin].sum(axis = 0, skipna = True), "\n")

            # Plot 2D mass histogram
            figure_name = "mass" + self.btag + str(bin)
            fig, ax = plt.subplots(num=figure_name)
            self.plotRegions(figure_name)
            plot2DHist(self.mass_hist_dict[lower_edge], figure_name=figure_name, cbar_log=True, output_file=self.output_dir+"massA_vs_massB_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))
            # Tag region
            figure_name = "mass" + self.btag + str(bin) + "tr"
            fig, ax = plt.subplots(num=figure_name)
            self.plotRegions(figure_name)
            plot2DHist(self.mass_hist_dict_tr[lower_edge], figure_name=figure_name, cbar_log=True, output_file=self.output_dir+"massA_vs_massB_TR_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))
            # Anti-tag region
            figure_name = "mass" + self.btag + str(bin) + "anti-tr"
            fig, ax = plt.subplots(num=figure_name)
            self.plotRegions(figure_name)
            plot2DHist(self.mass_hist_dict_tr[lower_edge], figure_name=figure_name, cbar_log=True, output_file=self.output_dir+"massA_vs_massB_anti-TR_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))

            # Plot the debug scatter plot
            figure_name = "debug" + self.btag + str(bin)
            self.plotRegions(figure_name)
            # Plot labels
            plt.xlabel("FatJetA %s mass [GeV]" % self.mass_text)
            plt.ylabel("FatJetB %s mass [GeV]" % self.mass_text)
            # Set style
            plt.xlim([0, 200])
            plt.ylim([0, 200])
            plt.gca().set_aspect('equal', adjustable='box') # Equal scale on x/y-axis
            # Save figure
            plt.savefig(self.output_dir+"massA_vs_massB_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))
            plt.close()

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        # Create branches that saves the signal mass region for the two fat jets
        self.out.branch("FatJets_signalRegion_%s" % (self.btag),"I")


    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass


    # Function for plotting the signal/sideband regions
    def plotRegions(self, figure_name):

        # Activate the specified figure
        plt.figure(figure_name)

        # The 10 mass region lines
        h_down = self.signal_h/(1+np.tan(np.pi/4-self.sideband_angle)) # The distance between each mass region line intersecting the down_line projected on the x-axis
        for i in range(self.num_signal_regions + 1):
            if i == 0:
                x_tmp, y_tmp = self.s1_lower_point_x, self.s1_lower_point_y
            else:
                x_tmp = self.sideband_lower_point_x + h_down * (i - 1)
                y_tmp = self.mass_line(x_tmp, i)
            x, y = [x_tmp, y_tmp], [y_tmp, x_tmp]
            plt.plot(x, y, 'k')

        # The "long" lines...
        x_signal_max = self.signal_h * self.num_signal_regions / (1+np.tan(np.pi/4-self.signal_angle)) - 1.1 # The x-coordinate for where the long signal region ends... with a correction factor as one bin is smaller. TODO: make a sensible correction...
        x1, y1 = [self.sideband_lower_point_x, x_tmp], [self.down_line(self.sideband_lower_point_x), y_tmp]
        x2, y2 = [self.s1_lower_point_x, self.s1_lower_point_x+x_signal_max], [self.signal_down_line(self.s1_lower_point_x), self.signal_down_line(self.s1_lower_point_x+x_signal_max)]
        x3, y3 = y2, x2
        x4, y4 = y1, x1

        plt.plot(x1, y1, 'k')
        plt.plot(x2, y2, 'k')
        plt.plot(x3, y3, 'k')
        plt.plot(x4, y4, 'k')

        # The special first sideband region lines
        x5, y5 = [self.s1_lower_point_x, self.sideband_lower_point_x], [self.d1_line(self.s1_lower_point_x), self.d1_line(self.sideband_lower_point_x)]
        x6, y6 = [self.s1_lower_point_y, self.sideband_lower_point_y], [self.u1_line(self.s1_lower_point_y), self.u1_line(self.sideband_lower_point_y)]

        plt.plot(x5, y5, 'k')
        plt.plot(x6, y6, 'k')


    # Run once for each event
    def analyze(self, event):

        # Compute the event weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # Get the two chosen fatjets
        fatjets = Collection(event, "FatJet") # AK8 jets
        fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
        fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]
        mass_a = getattr(fatjet_a, self.mass)
        mass_b = getattr(fatjet_b, self.mass)
        btag_a = getattr(fatjet_a, self.btag)
        btag_b = getattr(fatjet_b, self.btag)

        # Check that the fatjets are either in the btag or anti-btag region
        tag_region = False
        anti_tag_region = False
        ctrl_region = False

        if btag_a + btag_b > self.btag_sum_cut:
            tag_region = True
            if self.isData: # Safety in case it did not stop processing tag region data in btagCorrelationCheck 
                return False
        elif btag_a < self.btag_anti_cut and btag_b < self.btag_anti_cut:
            anti_tag_region = True
        elif (btag_a > self.btag_anti_cut and btag_a < self.btag_ctrl_cut) and (btag_b < (self.btag_range[0] + self.btag_ctrl_cut - self.btag_anti_cut)): # A square next to the anti-btag region
            ctrl_region = True
        elif (btag_b > self.btag_anti_cut and btag_b < self.btag_ctrl_cut) and (btag_a < (self.btag_range[0] + self.btag_ctrl_cut - self.btag_anti_cut)): # A square next to the anti-btag region
            ctrl_region = True

        # Find which mass signal region the jets potentially belong to. It checks the "upper"/"lower" edges. Region 0 corresponds to outside the area of interest
        signal_region = 0

        for n in range(self.num_signal_regions + 1):
            if mass_b < self.mass_line(mass_a, n):
                signal_region = n
                break

        # Decide which HT bin the event belongs to
        ht = event.HT
        ht_bin = ""

        for bin in self.ht_bin_edges[::-1]: # Loop in reverse
            if ht > bin:
                ht_bin = bin
                break

        if ht_bin == "": # HT less than the lowest bin edge
            return False

        self.nevents_passed_ht[ht_bin] += weight

        # Fill 2D mass histogram
        self.mass_hist_dict[ht_bin].fill(mass_a, mass_b, weight=weight)
        if tag_region:
            self.mass_hist_dict_tr[ht_bin].fill(mass_a, mass_b, weight=weight)
        elif anti_tag_region:
            self.mass_hist_dict_anti[ht_bin].fill(mass_a, mass_b, weight=weight)

        # Choose the debug plot to plot in
        plt.figure("debug" + self.btag + str(ht_bin))

        fill_signal_branch = False

        # Check the "sides" of the signal or sideband regions

        # Below the Down line, i.e. outside the area of interest
        if mass_b < self.down_line(mass_a) or signal_region == 0:
            plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
        # Inside the D sideband
        elif mass_b < self.signal_down_line(mass_a):
            if signal_region == 1 and mass_b < self.d1_line(mass_a):
                plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
            else:
                if tag_region:
                    self.df_dict[bin].at[signal_region, "D"] += weight
                    plt.plot(mass_a, mass_b, 'o', color=(0, 0, 1, signal_region/self.num_signal_regions)) # For debug
                elif anti_tag_region:
                    self.df_dict[bin].at[signal_region, "D_anti"] += weight
                    plt.plot(mass_a, mass_b, 'o', color=(1, 1, 0, signal_region/self.num_signal_regions)) # For debug
                elif ctrl_region:
                    self.df_dict[bin].at[signal_region, "D_ctrl"] += weight
                    plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, signal_region/self.num_signal_regions)) # For debug
                else:
                    plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
        # Inside the signal region
        elif mass_b < self.signal_up_line(mass_a):
            if tag_region:
                self.df_dict[bin].at[signal_region, "S"] += weight
                plt.plot(mass_a, mass_b, 'o', color=(0, 1, 0, signal_region/self.num_signal_regions)) # For debug
                fill_signal_branch = True
            elif anti_tag_region:
                self.df_dict[bin].at[signal_region, "S_anti"] += weight
                plt.plot(mass_a, mass_b, 'o', color=(1, 0, 1, signal_region/self.num_signal_regions)) # For debug
            elif ctrl_region:
                self.df_dict[bin].at[signal_region, "S_ctrl"] += weight
                plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, signal_region/self.num_signal_regions)) # For debug
            else:
                plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
        # Inside the U sideband
        elif mass_b < self.up_line(mass_a):
            if signal_region == 1 and mass_b < self.u1_line(mass_a):
                plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
            else:
                if tag_region:
                    self.df_dict[bin].at[signal_region, "U"] += weight
                    plt.plot(mass_a, mass_b, 'o', color=(1, 0, 0, signal_region/self.num_signal_regions)) # For debug
                elif anti_tag_region:
                    self.df_dict[bin].at[signal_region, "U_anti"] += weight
                    plt.plot(mass_a, mass_b, 'o', color=(0, 1, 1, signal_region/self.num_signal_regions)) # For debug
                elif ctrl_region:
                    self.df_dict[bin].at[signal_region, "U_ctrl"] += weight
                    plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, signal_region/self.num_signal_regions)) # For debug
                else:
                    plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
        # Above the Up line, i.e. outside the area of interest
        else:
            plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug

        if fill_signal_branch:
            self.out.fillBranch("FatJets_signalRegion_%s" % (self.btag), signal_region) # In the tag region
        else:
            self.out.fillBranch("FatJets_signalRegion_%s" % (self.btag), 0)

        return True
