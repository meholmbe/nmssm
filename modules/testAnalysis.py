#!/usr/bin/env python3
import hist
from hist import Hist

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import plot1DHist,plot2DHist

# Analysis module
# Creates histograms of HT etc.
class TestAnalysis(Module):
    def __init__(self, isData=False, btag="particleNet", ht_bin_edges=[1500, 2500, 3500], output_dir="./"):

        # Various constants and cuts
        self.isData = isData
        self.btag = btag # Which double b-tag discriminator to use. btagDDBvLV2 seems random
        self.ht_bin_edges = ht_bin_edges # The left edge of each HT bin. Default 1500-2500, 2500-3500, 3500+ GeV
        self.output_dir = output_dir+"/" # Output directory for plots


    # Run once at the beginning of the job
    def beginJob(self):

        # Set some variable depending on which b-tagger we're using
        if self.btag == "btagHbb":
            self.mass = "msoftdrop"
            self.btag_range = [-1, 1]
        elif self.btag == "particleNet":
            self.mass = "particleNet_mass"
            self.btag_range = [0, 1]
        else:
            raise ValueError("Invalid b-tagger.")


        # HT with cross-section scaling
        self.h_reco_ht = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="$Reco H_{T}$ [GeV]"), storage=hist.storage.Weight())

        if not self.isData:
            # Gen HT
            self.h_gen_ht = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="$Gen H_{T}$ [GeV]"), storage=hist.storage.Weight())
            # HT JES up
            self.h_reco_ht_up = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="Reco $H_{T}$ [GeV]"), storage=hist.storage.Weight())
            # HT JES down
            self.h_reco_ht_down = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="Reco $H_{T}$ [GeV]"), storage=hist.storage.Weight())


        # b-tag discriminator histograms, one per HT bin
        self.btag_hist_dict = {}
        for bin in self.ht_bin_edges:
            self.btag_hist_dict[bin] = hist.Hist(hist.axis.Regular(bins=100, start=self.btag_range[0], stop=self.btag_range[1], name="x", label="FatJetA " + self.btag + " discriminator"),
                                                 hist.axis.Regular(bins=100, start=self.btag_range[0], stop=self.btag_range[1], name="y", label="FatJetB " + self.btag + " discriminator"),
                                                 storage=hist.storage.Weight()
                                                )


    # Run once when all files have been processed
    def endJob(self):

        # Plot HT histogram
        plot1DHist(self.h_reco_ht, ylabel="Events", ylog=True, output_file=self.output_dir+"ht_reco_%s.png" % self.btag)

        # Plot Gen HT and JES histogram comparison
        if not self.isData:
            plot1DHist(self.h_gen_ht, ylog=True, ylabel="Events", output_file=self.output_dir+"ht_gen_%s.png" % self.btag)
            plot1DHist([self.h_reco_ht, self.h_reco_ht_up, self.h_reco_ht_down], ylabel="Events", legend_labels=["Default", "JES Up", "JES Down"], output_file=self.output_dir+"ht_JES_%s.png" % self.btag)

        # Plot b-tag discriminator plots
        for i in range(len(self.ht_bin_edges)):
            # String for specifying the HT bin in the plot title
            lower_edge = self.ht_bin_edges[i]
            upper_edge = ("-" + str(self.ht_bin_edges[i+1])) if(i < len(self.ht_bin_edges)-1) else "+"

            # Check that histograms has entries
            if not self.btag_hist_dict[lower_edge].sum().value:
                print("Warning: Histogram is empty.")
                continue

            # Plot 1D histogram
            plot1DHist(self.btag_hist_dict[lower_edge].project("x"), ylabel="Events", output_file=self.output_dir+"btagA_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))
            plot1DHist(self.btag_hist_dict[lower_edge].project("y"), ylabel="Events", output_file=self.output_dir+"btagB_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))

            # Plot 2D histogram, fraction of events per bin
            plot2DHist(self.btag_hist_dict[lower_edge]/self.btag_hist_dict[lower_edge].sum().value, cbar_log=True, cbar_label="Fraction of events per bin", output_file=self.output_dir+"btagA_vs_btagB_%s_HT%s%sGeV.png" % (self.btag, lower_edge, upper_edge))


    # Main analysis code
    # Run once for every event, return True (go to next module) or False (fail, go to next event)
    def analyze(self, event):

        # Compute total weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # jets = Collection(event, "Jet") # AK4 jets
        fatjets = Collection(event, "FatJet") # AK8 jets

        # Fill HT histograms
        self.h_reco_ht.fill(event.HT, weight=weight)
        if not self.isData:
            self.h_gen_ht.fill(event.GenHT, weight=weight)
            self.h_reco_ht_up.fill(event.HT_jesTotalUp, weight=weight)
            self.h_reco_ht_down.fill(event.HT_jesTotalDown, weight=weight)

        # The two chosen FatJets corresponding to b-quark decays
        fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
        fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]

        # Fill b-tag discriminator lists for the correct HT bin
        for bin in self.ht_bin_edges[::-1]: # Loop in reverse
            if event.HT > bin:
                self.btag_hist_dict[bin].fill(getattr(fatjet_a, self.btag), getattr(fatjet_b, self.btag), weight=weight)
                break

        return True
