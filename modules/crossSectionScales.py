#!/usr/bin/env python3
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Dictionaries of MC cross section scales and errors
# Taken from: https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns

# QCD cross section values for different HT bins (in pb).
xsec_qcd_dict = {
    "HT100to200": (27990000, 4073),
    "HT200to300": (1712000, 376.3),
    "HT300to500": (347700, 74.81),
    "HT500to700": (32100, 7),
    "HT700to1000": (6831, 1.7),
    "HT1000to1500": (1207, 0.5),
    "HT1500to2000": (119.9, 0.06),
    "HT2000toInf": (25.24, 0.02)
}

# TTJets cross section values
xsec_ttjets_dict = {
    "None": (831.76, 0), # Errors are specified as +19.77 -29.20 +35.06 -35.06...
    "HT-600to800": (0.7532, 0.005332),
    "HT-800to1200": (0.001430, 0.002196),
    "HT-1200to2500": (0.1316, 0.0003817),
    "HT-2500toInf": (0.001430, 0.000017)
}

# Saves the MC cross-sections for different HT bins
class CrossSectionScales(Module):
    def __init__(self):
        pass

    def beginJob(self):
        pass

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.input_file = inputFile.GetName()

        # Get the HT bin from the input file name
        if "HT" in self.input_file and ("QCD" in self.input_file or "TTJets" in self.input_file):
            self.gen_ht_bin = [x for x in self.input_file.split("_") if "HT" in x][0]
        else:
            self.gen_ht_bin = "None"

        # Create branches that saves the cross section for all events
        self.out.branch("xsec", "F")
        self.out.branch("xsec_err", "F")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        # Get the cross section value, or 1 if no HT bins?
        if "QCD" in self.input_file:
            xsec, xsec_err = xsec_qcd_dict[self.gen_ht_bin]
        elif "TTJets" in self.input_file:
            xsec, xsec_err = xsec_ttjets_dict[self.gen_ht_bin]
        else:
            xsec, xsec_err = 1, 0

        # Fill the branches
        self.out.fillBranch("xsec", xsec)
        self.out.fillBranch("xsec_err", xsec_err)

        return True
