#!/usr/bin/env python3
import random

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Saves the indices of the two chosen FatJets mostly likely corresponding to b quark decays.
# Returns False if it does not pass the kinematic requirements
class KinematicEventSelection(Module):
    def __init__(self, isData=False, btag="particleNet", ak8_pt_cut=300, ak4_pt_cut=300, ak8_eta_cut=2.4, ak4_eta_cut=3.0, deltaR_cut=1.4, output_dir="./"):

        self.isData = isData
        self.btag = btag # Which double b-tag discriminator to use. btagDDBvLV2 seems random
        self.ak8_pt_cut = ak8_pt_cut # GeV. Minimum jet pt for the chosen FatJets
        self.ak4_pt_cut = ak4_pt_cut # GeV. Minimum jet pt for at least one AK4 jet in each event.
        self.ak8_eta_cut = ak8_eta_cut
        self.ak4_eta_cut = ak4_eta_cut
        self.deltaR_cut = deltaR_cut # distance parameter between jets
        self.output_dir = output_dir + "/" # Output directory for plots


    def beginJob(self):

        # Count the number of events passed for debug
        self.nevents = 0 # Total number of events processed
        self.nevents_passed_ak8pt = 0 # Two fatjets found
        self.nevents_passed_ak4jet = 0 # Matching AK4 jet found
        self.nevents_passed = 0 # Total number of events passed

    def endJob(self):
        # Number of events passed
        print("\nTotal number of events (out of %i) that passed the Kinematic Event Selction (%s): %i" % (self.nevents, self.btag, self.nevents_passed))
        print("    - AK8 pT cut (>%i GeV): %i (%f%%)" % (self.ak8_pt_cut, self.nevents_passed_ak8pt, self.nevents_passed_ak8pt/self.nevents*100 if self.nevents else 0))
        print("    - Matching AK4 jet: %i (%f%%)" % (self.nevents_passed_ak4jet, self.nevents_passed_ak4jet/self.nevents*100 if self.nevents else 0))


    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        # Create branches that saves the indices for the two FatJets
        self.out.branch("FatJetIndexA_%s" % (self.btag),"I")
        self.out.branch("FatJetIndexB_%s" % (self.btag),"I")


    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass


    # Finds the two chosen FatJets corresponding to b quark decays.
    # Returns a list of two FatJet objects, highest b-tag discriminator value first.
    # Returns None if the fatjets does not satisfy the requirements.
    def chooseFatJets(self, event):

        fatjets = Collection(event, "FatJet") # AK8 jets

        fatjet_a, fatjet_b = None, None # The two chosen FatJets...
        index_a, index_b = None, None # ... and their position in the FatJet Collection

        # Choose FatJets, the two FatJets above 300 GeV with the largest b-tag discriminator value
        i = 0
        for fatjet in fatjets:
            if not fatjet.pt > self.ak8_pt_cut: continue
            if not abs(fatjet.eta) < self.ak8_eta_cut: continue # Additional to the pre-selection cut?
            btag_discr = getattr(fatjet, self.btag)
            if fatjet_a is None:
                fatjet_a, index_a = fatjet, i
            elif btag_discr > getattr(fatjet_a, self.btag):
                fatjet_b, index_b = fatjet_a, index_a
                fatjet_a, index_a = fatjet, i
            elif fatjet_b is None:
                fatjet_b, index_b = fatjet, i
            elif btag_discr > getattr(fatjet_b, self.btag):
                fatjet_b, index_b = fatjet, i
            i += 1

        return [[fatjet_a, index_a], [fatjet_b, index_b]]


    # Returns True if at least one of the AK4 jets pass the AK4 kinematic selection
    def passAK4Selection(self, event, fatjet_a, fatjet_b):

        jets = Collection(event, "Jet") # AK4 jets

        for j in jets:
            if not j.pt > self.ak4_pt_cut: continue
            if not abs(j.eta) < self.ak4_eta_cut: continue
            if not j.DeltaR(fatjet_a) > self.deltaR_cut: continue
            if not j.DeltaR(fatjet_b) > self.deltaR_cut: continue
            # Passed the selection
            return True

        return False


    # Run once for each event
    def analyze(self, event):

        # Compute the event weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # Count number of events
        self.nevents += weight

        # Choose FatJets corresponding to b-quark decay
        (fatjet_a, index_a), (fatjet_b, index_b) = random.sample(self.chooseFatJets(event), 2) # Shuffle the FatJets so the largest b-tag discriminator isn't always first

        # Stop processing if at least two FatJets does not pass the AK8 selection
        if fatjet_a is None or fatjet_b is None: # Stop processing this event if we can't find two FatJets
            return False
        self.nevents_passed_ak8pt += weight

        # Stop processing this event if no matching AK4 jets pass the selection
        if not self.passAK4Selection(event, fatjet_a, fatjet_b):
            return False
        self.nevents_passed_ak4jet += weight

        # Fill the branches with the FatJet index number
        self.out.fillBranch("FatJetIndexA_%s" % (self.btag), index_a)
        self.out.fillBranch("FatJetIndexB_%s" % (self.btag), index_b)

        self.nevents_passed += weight

        return True
