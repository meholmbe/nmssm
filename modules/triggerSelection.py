#!/usr/bin/env python3

from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module


# Select events based on triggers, only one out of a list of triggers need to be True for the event to pass
class TriggerSelection(Module):

    def __init__(self, triggers=None, year=None):

        self.triggers = triggers # What triggers to use
        self.year = year # Run year to define predefined triggers (gets overridden if triggers is defined)


    def beginJob(self):

        # Set which triggers to use
        if self.triggers is not None:
            pass
        elif self.year == 2016:
            self.triggers = ["HLT_PFHT900", "HLT_AK8PFJet450"]
        elif self.year == 2017:
            self.triggers = ["HLT_PFHT1050", "HLT_AK8PFJet500"]
        elif self.year == 2018:
            self.triggers = ["HLT_PFHT1050", "HLT_AK8PFJet500"]
        else:
            raise Exception("No list of triggers or valid run year (2016-2018) was defined.")

        # Count number of events
        self.nevents = 0 # Total number of events
        self.nevents_passed = 0 # Number of events passed


    def endJob(self):
        # Number of events passed
        print("\nTotal number of events (out of %i) that passed the Trigger Selction: %i\n" % (self.nevents, self.nevents_passed))


    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass


    # Run once for each event
    def analyze(self, event):

        # Count number of events
        self.nevents += 1

        # Check if speciified triggers triggered
        triggered = False
        for trigger in self.triggers:
            if getattr(event, trigger):
                triggered = True
                self.nevents_passed += 1
                break # Only one trigger needs to be True to pass

        return triggered
