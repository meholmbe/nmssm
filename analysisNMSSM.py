#!/usr/bin/env python3
import os
import argparse
import shutil

from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.jetHT import JetHT
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.btagCorrelationCheck import BTagCorrelationCheck
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.particleNetTagger import ParticleNetTagger
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.kinematicEventSelection import KinematicEventSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.massEventSelection import MassEventSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.weightsMC import WeightsMC
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.testAnalysis import TestAnalysis
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.roc import ROC
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.triggerSelection import TriggerSelection


# Argument parser
parser = argparse.ArgumentParser(description="Process some root file(s) and plot some hisograms to an output root file.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-i", "--input", default=None, nargs="+", help="Specifies the input file(s). Separate multiple root file inputs with a comma, or use a text file with the data file names. If no input files has been defined, search for files in inputDir (default = %(default)s)")
parser.add_argument("-I", "--inputDir", default="", help="Directory to the input files. Will take all matching files in the specified folder as input. Cannot be used at the same time as --input. (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default=None, help="Name of output directory. Defaults to input directory plots folder.")
parser.add_argument("-n", "--maxEntries", default=None, type=int, help="Maximum number of events to process, per input file... (default = %(default)s)")
parser.add_argument("-y", "--year", type=str, help="The year of the input files, e.g. 2018 (no UL). Used for deciding what triggers to use.", required=True)
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, btagHbb and/or particleNet, or all for all implemented b-taggers. (default = %(default)s)")
parser.add_argument("--isData", action="store_true", help="If input are data. If not specified, then MC events are assumed.")
parser.add_argument("--massSelectROC", action="store_true", help="If we want to run a seperate analysis that saves values for ROC curves after mass event selection.")
args = parser.parse_args()


# Input files to run over
# Only allow either --inputDir or --input as it will crash if we would have complete paths in the --input...
if args.inputDir and args.input:
    raise Exception("Only use either --inputDir or --input.")

if args.input is None: # Look for files in input directory
    inputs = [args.inputDir+"/"+f for f in os.listdir(args.inputDir) if (os.path.isfile(args.inputDir+"/"+f) and "_Skim.root" in f)]
elif ".root" not in args.input[0]: # Assume it's a text file that lists the input files. Currently only works for one input text file.
    with open(args.input[0], "r", encoding="utf-8") as file:
        inputs = file.read().splitlines()
elif args.inputDir == "":
    inputs = args.input
else:
    inputs = [args.inputDir+"/"+f for f in args.input]

# Check that we have inputs
if not inputs:
    raise FileNotFoundError("No inputs files were found.")
for file in inputs:
    # File does not exist locally or is not remote from xrootd
    if not os.path.exists(file) and not file.startswith("root:"):
        raise FileNotFoundError("Input file does not exist: %s" % file)

# Create output directory
if not args.outputDir and not args.inputDir:
    args.outputDir = "./results/"
elif not args.outputDir:
    args.outputDir = args.inputDir
if args.outputDir[-1] != "/": # Add slash if there is none
    args.outputDir += "/"
plot_dir = args.outputDir + "/plots/" # The output folder for plots
if os.path.exists(plot_dir): # Delete an existing plots folder, needed due to write permissions
    shutil.rmtree(plot_dir)
os.makedirs(plot_dir)

# Check that the taggers exist
btagger_list = ["particleNet", "btagHbb"]
if "all" in args.taggers:
    args.taggers = btagger_list
else:
    for t in args.taggers:
        if t not in btagger_list:
            raise Exception("The b-tagger(s) are not defined: %s" % t)

# Add any potential UL in year
args.year = int(args.year.replace("UL", ""))


################################
#         Particle Net         #
################################

# Run analysis using particleNet b-tagging
if "particleNet" in args.taggers:

    # Some constants
    gen_ht_min = 0 # GeV. Minimum reco-level HT to process
    gen_ht_max = float("inf") # GeV. Maximum reco-level HT to process
    ht_bin_edges = [0]
    btag_cut = 0.15 # For one-dimensional b-tag correlation check
    btag_sum_cut = 1.8
    btag_anti_cut = 0.6
    btag_ctrl_cut = 0.9
    tagger = "particleNet"

    # Create a chain of modules to run for the analysis
    analysis_chain_particleNet = []

    # Add trigger selection
    analysis_chain_particleNet.append(TriggerSelection(year=args.year))

    # Set weights for the run
    if not args.isData:
        analysis_chain_particleNet.append(WeightsMC(input_files=inputs, maxEntries=args.maxEntries, extra_weight=None))

    # Add HT modules to analysis chain. Potentially do this in post processing step
    analysis_chain_particleNet.append(JetHT(gen_ht_min=gen_ht_min, gen_ht_max=gen_ht_max, isData=args.isData))
    if not args.isData:
        analysis_chain_particleNet.append(JetHT(pt="pt_jesTotalUp"))
        analysis_chain_particleNet.append(JetHT(pt="pt_jesTotalDown"))

    # Some quick btag-mass correlation check. Also removes signal region events if data sample
    analysis_chain_particleNet.append(ParticleNetTagger()) # Compute btag for particle net
    analysis_chain_particleNet.append(BTagCorrelationCheck(isData=args.isData, output_dir=plot_dir, btag=tagger, btag_cut=btag_cut, btag_sum_cut=btag_sum_cut))

    # Add kinematic event selection
    analysis_chain_particleNet.append(KinematicEventSelection(isData=args.isData, output_dir=plot_dir, btag=tagger))

    # Add mass event selection
    if not args.isData and args.massSelectROC: # If we want to get values for a ROC curve
        analysis_chain_particleNet.append(MassEventSelection(isData=args.isData, output_dir=plot_dir, btag=tagger, ht_bin_edges=ht_bin_edges, btag_sum_cut=-10, btag_anti_cut=btag_anti_cut, btag_ctrl_cut=btag_ctrl_cut))
        analysis_chain_particleNet.append(ROC(output_dir=plot_dir, btag=tagger)) # Add module which loops over btag sum cuts and checks efficiency
    else:
        analysis_chain_particleNet.append(MassEventSelection(isData=args.isData, output_dir=plot_dir, btag=tagger, ht_bin_edges=ht_bin_edges, btag_sum_cut=btag_sum_cut, btag_anti_cut=btag_anti_cut, btag_ctrl_cut=btag_ctrl_cut))

    # Add TestAnalysis module
    analysis_chain_particleNet.append(TestAnalysis(isData=args.isData, output_dir=plot_dir, btag=tagger, ht_bin_edges=ht_bin_edges))

    # Create post processor and run it
    p_particleNet = PostProcessor(args.outputDir, inputFiles=inputs,
                      modules=analysis_chain_particleNet,
                      maxEntries=args.maxEntries,
                      # noOut=True,
                      friend=True, # Don't want an output root file, but otherwise I can't access values between different modules?
                      postfix="_Friend_"+tagger,
                      )
    p_particleNet.run()

    # Print out the cuts used for the run
    print("\nMinimum value for the b-tag: %f" % btag_cut)
    print("Tag region, minimum value of the b-tag sum: %f" % btag_sum_cut)
    print("Upper limit of the anti b-tag region: %f" % btag_anti_cut)
    print("Upper limit of the b-tag control region (lower limit is the anti b-tag region): %f\n" % btag_ctrl_cut)


################################
#            btagHbb           #
################################

# Run analysis using the old btagHbb
if "btagHbb" in args.taggers:

    # Some constants
    gen_ht_min = 0 # GeV. Minimum reco-level HT to process
    gen_ht_max = float("inf") # GeV. Maximum reco-level HT to process
    ht_bin_edges = [0]
    btag_cut = 0.3 # For one-dimensional b-tag correlation check
    btag_sum_cut = 1.3
    btag_anti_cut = 0.3
    btag_ctrl_cut = 0.8
    tagger = "btagHbb"

    # Create a chain of modules to run for the analysis
    analysis_chain_btagHbb = []

    # Add trigger selection
    analysis_chain_btagHbb.append(TriggerSelection(year=args.year))

    # Set weights for the run
    if not args.isData:
        analysis_chain_btagHbb.append(WeightsMC(input_files=inputs, maxEntries=args.maxEntries, extra_weight=None))

    # Add HT modules to analysis chain. Potentially do this in post processing step
    analysis_chain_btagHbb.append(JetHT(gen_ht_min=gen_ht_min, gen_ht_max=gen_ht_max, isData=args.isData))
    if not args.isData:
        analysis_chain_btagHbb.append(JetHT(pt="pt_jesTotalUp"))
        analysis_chain_btagHbb.append(JetHT(pt="pt_jesTotalDown"))

    # Some quick btag-mass correlation check. Also removes signal region events if data sample
    analysis_chain_btagHbb.append(BTagCorrelationCheck(isData=args.isData, output_dir=plot_dir, btag=tagger, btag_cut=btag_cut, btag_sum_cut=btag_sum_cut))

    # Add kinematic event selection
    analysis_chain_btagHbb.append(KinematicEventSelection(isData=args.isData, output_dir=plot_dir, btag=tagger))

    # Add mass event selection
    if not args.isData and args.massSelectROC: # If we want to get values for a ROC curve
        analysis_chain_btagHbb.append(MassEventSelection(isData=args.isData, output_dir=plot_dir, btag=tagger, ht_bin_edges=ht_bin_edges, btag_sum_cut=-10, btag_anti_cut=btag_anti_cut, btag_ctrl_cut=btag_ctrl_cut))
        analysis_chain_btagHbb.append(ROC(output_dir=plot_dir, btag=tagger)) # Add module which loops over btag sum cuts and checks efficiency
    else:
        analysis_chain_btagHbb.append(MassEventSelection(isData=args.isData, output_dir=plot_dir, btag=tagger, ht_bin_edges=ht_bin_edges, btag_sum_cut=btag_sum_cut, btag_anti_cut=btag_anti_cut, btag_ctrl_cut=btag_ctrl_cut))

    # Add TestAnalysis module
    analysis_chain_btagHbb.append(TestAnalysis(isData=args.isData, output_dir=plot_dir, btag=tagger, ht_bin_edges=ht_bin_edges))

    # Create post processor and run it
    p_btagHbb = PostProcessor(args.outputDir, inputFiles=inputs,
                      modules=analysis_chain_btagHbb,
                      maxEntries=args.maxEntries,
                      # noOut=True,
                      friend=True, # Don't want an output root file, but otherwise I can't access values between different modules?
                      postfix="_Friend_"+tagger,
                      )
    p_btagHbb.run()

    # Print out the cuts used for the run
    print("\nMinimum value for the b-tag: %f" % btag_cut)
    print("Tag region, minimum value of the b-tag sum: %f" % btag_sum_cut)
    print("Upper limit of the anti b-tag region: %f" % btag_anti_cut)
    print("Upper limit of the b-tag control region (lower limit is the anti b-tag region): %f\n" % btag_ctrl_cut)


# Could have more PostProcessors? Using the same input/output files, does not overwrite and still have access to the new branches?
