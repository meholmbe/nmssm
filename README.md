# NMSSM

A Next-to-Minimal Supersymmetric Standard Model (NMSSM) analysis for CMS Run 3. This analysis aims to search for light Higgs bosons...

## Contents of Repository

- `postProcNMSSM.py` calculates new variables needed for the analysis and adds them to a new output root file.
- `analysisNMSSM.py` uses the output root file from the above script to make some histograms (e.g. HT, jet energy scale systematics, b-tag correlation...). _Note: currently it creates a Friend tree as I couldn't figure out an easier way for modules in the chain to access the same values._
- `requirements.txt` list of packages and version number that are required.
- `QCUT_study` stand-alone study.

#### Modules

The `modules/` folder contains processing modules needed for the analysis that is not part of the NanoAOD-Tools:

- `btagCorrelationCheck.py` creates some plots that compares the correlation between the chosen b-tagger and mass. Also prints out the b-tagging efficiency values for different b-tag cuts to a `.dat` file in the output folder. These values can be used to plot ROC curves using `/utilities/plotROC.py`.
- `crossSectionScales.py` adds a branch with the cross-section scales to the output `root` file. Values are taken from the [summary table of samples](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns).
- `jetHT.py` computes HT using AK4 jets and adds them to the output tree. Default pT and eta cuts can be changed, as well as which pT branch to use.
- `kinematicEventSelection.py` chooses the two FatJets (AK8) with the largest b-tag discriminator values that passes the kinematic event selection and adds their indices to the output tree as FatJetIndexA and FatJetIndexB. The two indices are not sorted in any way, i.e. which FatJet gets to be A and B is randomised.
- `massEventSelection.py` uses the two FatJets chosen by the kinematic event selection to count the number events that are located in signal and sideband regions. These numbers are then used for the data driven QCD estimation.
- `particleNetTagger.py` adds a branch with the computed b-tag discriminator using ParticleNet.
- `roc.py` writes values for creating ROC curves after the mass event selection (in the same `.dat` as `btagCorrelationCheck.py`). It is only run if the `--massSelectROC` flag is used as it overrides the default b-tag sum cut in the mass event selection, and applies the various b-tag cuts in this module instead.
- `testAnalysis.py` makes some random HT histograms.
- `triggerSelection.py` select events depending on a list of triggers.
- `weightsMC.py` computes the final weight of each MC event, using the integrated luminosity, generator weight, and cross-section scales. The weight is saved as the branch `weights_mc`.

#### Utilities

The `utilities/` folder contains various helper functions:

- `nanoAODTools_python3_debug.sh` edits some of the nanoAOD-tools files to fix bugs when going from `python2` to `python3`.
- `plotROC.py` a stand-alone ROC curve plotting script that takes the b-tag efficiency `.dat` files from running the analysis as inputs. See "Plot ROC Curves" section for further details.
- `plotSearchRegionBins.py` a stand-alone plotting script for creating a stacked histogram of the search region bins for signal and/or background samples.
- `plotUtils.py` contains functions for plotting histograms.
- `submitCondorJobs.py` is a python script that submits jobs to HTCondor.
- `crab/` folder containing scripts for running the postprocessing step using CRAB. See intructions below.
- `Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt` the Golden JSON file for analysing data.

## Checkout Instructions

Start by checking out CMSSW and nanoAOD-tools, according to the [nanoAOD-tools repository](https://github.com/cms-nanoAOD/nanoAOD-tools):

    cmsrel CMSSW_12_3_0_pre2
    cd CMSSW_12_3_0_pre2/src
    cmsenv
    git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools
    cd PhysicsTools/NanoAODTools

Now clone the NMSSM analysis repository and compile:

    git clone https://gitlab.cern.ch/gsalvi/nmssm.git python/postprocessing/NMSSM
    scram b

Unclear what difference the CMSSW release version makes when using NanoAOD-Tools, but the later `CMSSW_10_X_Y`, `CMSSW_11_X_Y` and `CMSSW_12_X_Y` with `python3` does not seem to work without some changes done by the following `bash` script:

    ./python/postprocessing/NMSSM/utilities/nanoAODTools_python3_debug.sh

Install the corret package version:

    pip3 install -r requirements.txt --user

Make sure there is a proxy to access files on the grid:

    voms-proxy-init --voms cms


## Run the Analysis

    python3 python/postprocessing/NMSSM/postProcNMSSM.py -i python/postprocessing/NMSSM/test/test_qcd_background_files.txt -O out_post_proc -n 1000 -y UL2017
    python3 python/postprocessing/NMSSM/analysisNMSSM.py --inputDir out_post_proc -t all -y 2017

This runs the post-processing directly on the machine you are on, which is okay for a couple of thousand events while doing tests. However, for large datasets it is possible to run the post-processing using HTCondor or CRAB, see below.

### Options for postProcNMSSM

- `--input` `-i` specify input files directly separated by commas or load a text file with the input files listed. If input files from the grid wants to be used, start the path with `root://cms-xrd-global.cern.ch/`.
- `--outputDir` `-O` specify the directory for the output files. Defaults to current directory.
- `--maxEvents` `-n` maximum number of events to process **per input file**.
- `--year` `-y` the run year of the input file. Normally specified in the file name.
- `--jsonInput` `-j` specifies a JSON file for event selection. Defaults to None if processing MC, but defaults to the "Golden JSON" file stored in `utilities\Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt`.
- `--isData` specified that input files are data samples. Default is False, but if file names contain `store/data/` it is automatically set to True.

Outputs one `*_Skim.root` file per input file.

### Options for analysisNMSSM

- `--input` `-i` specify input files directly separated by commas or load a text file with the input files listed.
- `--inputDir`, `-I` specify directory that contains all the `*_Skim.root` files from running `postProcNMSSM.py`.
- `--outputDir` `-O` specify the directory for the output files. Defaults to input directory `plots/` folder.
- `--maxEvents` `-n` maximum number of events to process **per input file**.
- `--year` `-y` The year of input files, e.g. 2018 (no UL), Used for deciding which triggers to use.
- `--taggers` `-t` specifies which b-taggers to run the analysis with (btagHbb or particleNet). `all` runs all b-taggers implemented. 
- `--isData` specified that input files are data samples. Default is False.
- `--massSelectROC` runs a seperate analysis that saves values for creating ROC curves after mass event selection.

Outputs `*_Friend.root` files containing stuff needed during the run but not afterwards in the specified output directory, and some histograms in the directory from where the script was run.

## Run with HTCondor

For larger datasets it is better to submit jobs to HTCondor, using the same arguments as above, but with and additional `--runPostProc` or `--runAnalysis`:

    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -i python/postprocessing/NMSSM/test/test_qcd_background_files.txt -O out_post_proc -n 1000 -y UL2017 --runPostProc
    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -i python/postprocessing/NMSSM/test/test_qcd_background_files.txt -O out_post_proc -n 1000 -t all -y 2017 --runAnalysis

_Note: Currently doesn't split the job up into multiple smaller jobs..._

## Run with CRAB

It is also possible to submit jobs using CRAB when processing very large datasets:

    crab submit -c python/postprocessing/NMSSM/utilities/crab/crab_cfg.py

_Note: Needs updataing..._
This writes the output to RAL linux machines in `/pnfs/pp.rl.ac.uk/data/cms/store/user/<YOUR CERN USERNAME>`. Then run `analysisNMSSM.py` on the output from CRAB (locally?).

To change configuration, e.g. datasets or number of jobs, edit `crab_cfg.py`. The file `crabPostProc.py` corresponds to the `postProcNMSSM.py`.

## Plot Search Region Bin Numbers

There is a stand-alone script to plot stacked histograms of search region (signal mass) bin numbers in the utilities folder: `utilities/plotROC.py`. The script uses the Friend root files created by `analysisNMSSM.py` as input. Note that this only works with MC samples, and that one has to run `analysisNMSSM.py` multiple times to create all the input files needed for the stack. 

    python3 python/postprocessing/NMSSM/utilities/plotSearchRegionBins.py -S signal_directory -s signal_name -B background_directory -b background_name

Multiple signals and backgrounds can be added, but the number of signal (background) names and signal (backround) directories must be the same. The signal and background names are used for the stack legend.

### Options for plotSearchRegionBins.py

    - `--signalDirs` `-S` the input directories with signal files.
    - `--signalNames` `-s` the names of the signal files, for plot legend.
    - `--backgroundDirs` `-B` the input directories with background files.
    - `--backgroundNames` `-b` the names of the background files, for plot legend.
    - `--outputDir` `-O` the output directory (has to already exist).
    - `--output` `-o` the output file name.
    - `--taggers` `-t` specify b-tagger to run on, either btagHbb or particleNet, or all for both

## Plot ROC Curves

There is a stand-alone script to plot ROC curves of the b-tagging efficiency in the utilities folder: `utilities/plotROC.py`. The script uses the `btag_efficiency_*.dat` files created by `module/btagCorrelationCheck.py` as input, which contains the b-tag cuts with the corresponding values for the True Positive (TP) and False Positive (FP) numbers. Note that this only works with MC samples, and that one has to run `module/btagCorrelationCheck.py` (included when running `analysisNMSSM.py`) multiple times to create all the input files needed. I.e. one time with signal samples for the TP input, and one time for each background sample as the FP inputs.

The `btag_efficiency_*_1D.dat` contains the number of TP/FP for tagging every b-jet in an event, while `btag_efficiency_*_2D.dat` contains the number of TP/FP for tagging two true b-jets per event. The former also contains the total number of jets and b-jets, while the latter contains the total number of events.

    python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp <TP_FILE_PATH>/btag_efficiency_btagHbb_1D.dat -fp <FP_FILE_PATH>/btag_efficiency_btagHbb_1D.dat <ANOTHER_FP_FILE_PATH>/btag_efficiency_btagHbb_1D.dat -l ttbar QCD -o roc_btagHbb_1D.png

Note that the TP and FP values are in general only true for signal and background samples, respectively. For the signal sample, all b-jets are assumed to have come from a Higgs decay, and all b-jets in the background sample are assumed to be FP. Therefore, the real number of FP in background samples is actually the number of FP+TP.

### Options for plotROC.py

- `--inputTP` `-tp` specifies the TP input file path. Required.
- `--inputFP` `-fp` specifies the FP input file path(s). Multiple files can be specified separated by a space. Required.
- `--legend` `-l` specifies the description/label for each FP input file to be used in the plot legends. Each label should be separated with a space and written in the order of the FP input files. Required.
- `--output` `-o` name of output file containing the ROC curve plot.
- `--outputDir` `-O` specifies the directory for output files.
- `--massSelectROC` creates ROC curves using values after mass event selection. Need to have run `analysisNMSSM.py` with the `--massSelectROC` flag.

## Known Issues

#### CRAB ImportError in job_out.txt

    ImportError: This package should not be accessible on Python 3. Either you are trying to run from the python-future src folder or your installation of python-future is corrupted.

After CRAB has run the script using `python3` and returned a successful error code, it tries to run an additional thing (unclear what) that calls a `python2` module which causes an error. As the job has already been marked as succesful, and written the output file, this error found in the jobs `job_out*.txt` log can be ignored.


#### Unknown branch Muon_isGlobal

    RuntimeError: Unknown branch Muon_isGlobal

Error caused by `isGlobal` branch not existing in the NanoAOD input file. Tends to happen more frequently if old NanoAOD files are used, try to use files created with NanoAOD version 5 (?) or higher.

## Datasets

1. [QCD sample for background estimation testing](https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fglobal&input=dataset%3D%2FQCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8%2F*%2FNANOAODSIM)
2. [TTbar sample for background estimation testing](https://cmsweb.cern.ch/das/request?instance=prod/global&input=file+dataset%3D%2FTTJets_HT-2500toInf_TuneCP5_13TeV-madgraphMLM-pythia8%2FRunIIAutumn18NanoAODv7-Nano02Apr2020_102X_upgrade2018_realistic_v21-v1%2FNANOAODSIM)
3. [Signal sample for testing](https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fglobal&input=dataset%3D%2F*ToHHTo4B*%2F*NanoAODv9*2018*%2FNANOAODSIM)
4. [Data sample for testing](https://cmsweb.cern.ch/das/request?input=dataset%3D%2FJetHT%2FRun2018A-UL2018_MiniAODv2_NanoAODv9-v2%2FNANOAOD&instance=prod/global)

## Papers
1. [Light Higgs paper](http://cms.cern.ch/iCMS/analysisadmin/get?analysis=HIG-20-018-paper-v22.pdf)

## Useful Links

1. [NanoAOD-tools repository](https://github.com/cms-nanoAOD/nanoAOD-tools)
2. [NanoAOD-tools Twiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/NanoAODTools)
3. [List of NanoAOD file contents](https://cms-nanoaod-integration.web.cern.ch/integration/cms-swCMSSW_10_6_X/mc106Xul17v2_doc.html)
4. [Summary table of samples](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns)
5. [CRAB Commands](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3Commands)
6. [CRAB Configuration File Documentation](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile)
7. [General advice for analysing MC and data](https://twiki.cern.ch/twiki/bin/view/CMS/PdmV)
8. [Datasets for Analysis](https://twiki.cern.ch/twiki/bin/view/CMS/PdmVDatasetsUL2018)
9. [Golden JSON and other potentially useful stuff](https://twiki.cern.ch/twiki/bin/view/CMS/PdmVLegacy2018Analysis#Data_Certification)
10. [Official Integrated Luminosity](https://twiki.cern.ch/twiki/bin/edit/CMS/PdmVRun2LegacyAnalysis)
11. [List of triggers (Run 2)](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HLTPathsRunIIList#2018)
12. [More information of trigger cuts etc.](https://hlt-config-editor-confdbv3.app.cern.ch/open?cfg=%2Fcdaq%2Fphysics%2FRun2018%2F2e34%2Fv3.6.1%2FHLT%2FV2&db=online)
